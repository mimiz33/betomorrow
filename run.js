/**
 * The Fourth Solution in a Functional format
 */
var parser = require('./parser.js');
var fs = require('fs');

var file = "exercice.txt";

/**
 * Print the square found.
 *
 * @param A The A coordinates
 * @param B The B coordinates
 * @param C The C coordinates
 * @param D The D coordinates
 */
function log(A,B,C,D){
    console.log("There is a SQUARE : ");
    console.log(A.toString());
    console.log(B.toString());
    console.log(C.toString());
    console.log(D.toString());
    return true;
}


/**
 * Calculate the coordinates of the vector.
 *
 * @param A The A coordinates
 * @param B The B coordinates
 * @returns [u,v]
 */
function vectorify(A, B) {
    return [B[0] - A[0], B[1] - A[1]];
}

/**
 * Indicates if the points A and B are equals
 *
 * @param A The A coordinates
 * @param B The B coordinates
 * @returns {boolean}
 */
function superposed(A, B) {
    return !!(A[0] === B[0] && A[1] === B[1]);
}


/**
 * Checks if a Square exists for points A and B
 *
 * @param A The A coordinates
 * @param B The B coordinates
 * @param exists the parser function that indicates the point exists in the collection
 * @returns {boolean} true if
 */
function checkSquareInMapFrom(A, B, exists) {

    var vector = vectorify(A,B);
    var C = [B[0] + vector[1], B[1] - vector[0]];
    var D = [C[0] - vector[0], C[1] - vector[1]];
    if (exists(C) && exists(D)) {
        log(A,B,C,D);
        return true;
    }
}



/**
 * Find squares from a single point A.
 * Encapsulate the function iteration and avoid testing the superposed points (equality)
 *
 * @param A The A coordinates
 * @param exists the parser function that indicates the point exists in the collection
 * @returns {Function} The returned function takes the B point as parameter, to check squares from [AB] segment.
 */
function findSquaresFrom(A, exists) {
    return function (B) {
        if (!superposed(A, B)) {
            return checkSquareInMapFrom(A, B, exists);
        }
    }
}

/**
 * Iterates over the points to find Square.
 *
 * @param points The array of points
 * @param pointExists the parser function that indicates if the point exists in the collection
 * @returns {*}
 */
function isThereASquare(points, pointExists) {
    return points.some(function (A) {
        return points.some( findSquaresFrom(A, pointExists) );
    });
}


/**
 * Retrieve file from command line
 */
if( process.argv.length === 3 ){
    file = process.argv[2];
}

fs.exists(file, function (isFile) {
    if (isFile) {
        console.log("Checking for square");

        // Parse the file and look for square
        parser.parse(file, isThereASquare);

    } else {
        console.log("ERROR : %s does not exists", file);
        console.log("");
        console.log("Usage is : node run.js DATA_FILE");
        console.log("  DATA_FILE : is the file that containing points ...");
        console.log("              Default file is ./exercice.txt");
    }
});
var fs = require('fs');

// The collection to check if the point exists.
var mapPoints = {};

/**
 * Check if the point is in the collection (it must be an array)
 * This function is private to the module.
 *
 * @param P
 * @returns {boolean}
 */
function exists(P) {
    if (Array.isArray(P)) {
        return mapPoints[P.join(' ')] ? true : false;
    }
    throw "Exists function, Parameter P must be an array";
}

/**
 * Process Data from file and convert to array of string (each element is a line of the file).
 *
 * @param data The data from file
 * @returns {Array} The array of lines (string)
 */
function convertDataFileToArrayOfLines(data) {
    return data.toString().split("\n");
}

/**
 * Convert the string representing a point into an array of int ( "xa ya" => [Ax, Ay]).
 *
 * @param str The point in a string format
 * @returns {Array} The point in an array of int format
 */
function convertStringPointToArrayOfInt(str) {
    var exploded = str.split(' ');
    exploded = exploded.map(function (el) {
        return parseInt(el, 10)
    });
    return exploded;
}

/**
 * Process the Lines, return an array of points ([[int,int],[int,int],...[int,int]] ).
 *
 * @param lines
 * @returns {Array} The point collection
 */
function processLines(lines) {
    var points = [];
    for (var i = 0; i < lines.length; i++) {
        if (lines[i].trim() !== '') {
            points.push(convertStringPointToArrayOfInt(lines[i]));
            mapPoints[lines[i]] = true;
        }
    }
    return points;
}

/**
 * Process the Lines and call the Callback
 *
 *
 * @param lines Array of lines in a String format
 * @param callback The Callback to call with lines processed.
 */
function processLinesAndCall(lines, callback) {
    callback(processLines(lines), exists);
}

/**
 * this function parse the data file and call the callback function two parameters
 *  - points which represents a list of all points in an array format : [[x1,y1][x2,y2]...]
 *  - exists, a function that checks if the parameter point exists in collection. (This avoid to expose the map)
 *
 * @param file The file with data
 * @param callback The callback that will treat the result
 */
exports.parse = function (file, callback) {

    fs.readFile(file, function (err, data) {
        if (err) throw err;
        processLinesAndCall(convertDataFileToArrayOfLines(data), callback);
    });
};